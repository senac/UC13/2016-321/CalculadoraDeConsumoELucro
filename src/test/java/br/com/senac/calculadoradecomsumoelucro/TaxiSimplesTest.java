package br.com.senac.calculadoradecomsumoelucro;

import static org.junit.Assert.*;
import org.junit.Test;

public class TaxiSimplesTest {

    @Test
    public void deveConsumir1ParaCorridaDe10Km() {
        TaxiSimples taxi = new TaxiSimples(0, 10, 10, 100);
        double consumo = taxi.getConsumo();
        assertEquals(1, consumo, 0);
    }

    @Test
    public void deveObteLucroDe81ParaCorridaDe10Km() {
        TaxiSimples taxi = new TaxiSimples(0, 10, 10, 100);
        double lucro = taxi.getLucro();
        assertEquals(81, lucro, 0);
    }

    @Test
    public void deveConsumir3EMeioParaCorridaDe140Km() {
        TaxiSimples taxi = new TaxiSimples(10, 150, 40.5, 200);
        double consumo = taxi.getConsumo();
        assertEquals(3.45, consumo, 0.01);
    }

    @Test
    public void deveObteLucroDe1232ParaCorridaDe140Km() {
        TaxiSimples taxi = new TaxiSimples(10, 150, 40.5, 200);
        double lucro = taxi.getLucro();
        assertEquals(123.05, lucro, 0);
    }

}
