package br.com.senac.calculadoradecomsumoelucro;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TaxiComplexoTest {
    
    @Test
    public void deveConsumir1ParaCorridaDe10Km() {
        
        TaxiComplexo taxiComplexo = new TaxiComplexo(0, 10);
        taxiComplexo.novaCorrida(10, 100);
        double consumo = taxiComplexo.getConsumo();        
        assertEquals(1, consumo, 0);
        
    }

    //@Test
    public void deveObteLucroDe81ParaCorridaDe10Km() {
        TaxiComplexo taxiComplexo = new TaxiComplexo(0, 10);
        taxiComplexo.novaCorrida(10, 100);
        double lucro = taxiComplexo.getLucro();        
        assertEquals(81, lucro, 0);
    }
    
    
      @Test
      public void deveConsumir3EMeioParaCorridaDe140Km() {
        
        TaxiComplexo taxiComplexo = new TaxiComplexo(10, 40.5);
        taxiComplexo.novaCorrida(65, 25);
        taxiComplexo.novaCorrida(10, 105);
        taxiComplexo.novaCorrida(50, 55);
        taxiComplexo.novaCorrida(15, 15);
        
        double consumo = taxiComplexo.getConsumo();        
        assertEquals(3.45, consumo, 0.01);
        
    }

    //@Test
  public void deveObteLucroDe1232ParaCorridaDe140Km() {
        TaxiComplexo taxiComplexo = new TaxiComplexo(10, 40.5);
        taxiComplexo.novaCorrida(65, 25);
        taxiComplexo.novaCorrida(10, 105);
        taxiComplexo.novaCorrida(50, 55);
        taxiComplexo.novaCorrida(15, 15);
        double lucro = taxiComplexo.getLucro();        
        assertEquals(123.05, lucro, 0.01);
    }
    
    
    
}
