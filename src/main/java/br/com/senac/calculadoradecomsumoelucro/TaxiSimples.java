package br.com.senac.calculadoradecomsumoelucro;

public class TaxiSimples {

    private double kmInicial;
    private double kmFinal;
    private double litros;
    private double valorTotal;

    public TaxiSimples() {
    }

    public TaxiSimples(double kmInicial, double kmFinal, double litros, double valorTotal) {
        this.kmInicial = kmInicial;
        this.kmFinal = kmFinal;
        this.litros = litros;
        this.valorTotal = valorTotal;
    }

    public double getKmInicial() {
        return kmInicial;
    }

    public void setKmInicial(double kmInicial) {
        this.kmInicial = kmInicial;
    }

    public double getKmFinal() {
        return kmFinal;
    }

    public void setKmFinal(double kmFinal) {
        this.kmFinal = kmFinal;
    }

    public double getLitros() {
        return litros;
    }

    public void setLitros(double litros) {
        this.litros = litros;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }
    
    public double getConsumo(){
        return (this.kmFinal - this.kmInicial) / this.litros ; 
    }
    
    public double getLucro(){
        return this.valorTotal - (this.litros*1.9) ;
    }

}
